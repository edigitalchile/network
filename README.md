# Modulo de terraform para crear el sistema de redes en aws

Este modulo esta basado en el modulo oficial de terraform para la creacion y confuguracion de redes en aws: https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/2.64.0


### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| azs  | Lista de zonas de disponibilidad donde que se quieren crear recursos  | SI  | N/A  |
| cidr  | Red de la vpc en formato x.x.x.x/x  | SI  | N/A  |
| vpc_name  | Nombre de la vpc  | SI  | N/A  |
| private_subnet_start  | Punto de inicio de la subred privada  | NO  | 1  |
| public_subnet_start  | Punto de inicio de la subred public  | NO  | 100  |
| database_subnet_start  | Punto de inicio de la subred public  | NO  | 200  |
| private_bit_mask  | Mascara de la subred privada  | NO  | 8  |
| public_bit_mask  | Mascara de la subred publica  | NO  | 8  |
| database_bit_mask  | Mascara de la subred de la base de datos  | NO  | 8  |
| global_tags  | Tags que se asociaran a todos los recursos  | NO  | {}  |
| enable_dns_hostnames  | Habilitar dns hostname en la vpc  | NO  | true  |
| create_private_subnets  | Habilitar la creacion de subredes privadas  | NO  | true  |
| create_public_subnets  | Habilitar la creacion de subredes publicas  | NO  | true  |
| create_database_subnets  | Habilitar la creacion de subredes privadas  | NO  | true  |
| enable_nat_gateway  | Habilitar creacion de la nat gateway  | NO  | true  |
| single_nat_gateway  | Configurar solo una nat gateway en toda la vpc  | NO  | true  |
| one_nat_gateway_per_az  | Configurar solo una nat gateway por zona de disponibilidad  | NO  | false  |
| create_igw  | Crear internet gateway  | NO  | false  |
| create_peering  | Crear peering entre dos vpc  | NO  | false  |
| peering_vpcs_ids  | Ids de las vpc que deben aceptar el peering  | NO  | null  |
| auto_accept_peering  | Aceptar automaticamente el peering. Las vpc deben estar en la misma cuenta aws  | NO  | true  |
| peering_requester_public_route  | Agregar ruta a tabla de ruteo publica de la vpc  | NO  | false  |
| peering_requester_private_route  | Agregar ruta a tabla de ruteo privada de la vpc  | NO  | false  |
| peering_requester_database_route  | Agregar ruta a tabla de ruteo de base de datos de la vpc  | NO  | false  |
| peering_accepter_public_route  | Agregar ruta a tabla de ruteo publica de la vpc  | NO  | false  |
| peering_accepter_private_route  | Agregar ruta a tabla de ruteo privada de la vpc  | NO  | false  |
| peering_accepter_database_route  | Agregar ruta a tabla de ruteo de la base de datos de la vpc  | NO  | false  |
| accepter_public_route_table_ids  | Lista de ids de tablas de ruteo publicas  | NO  | []  |
| accepter_private_route_table_ids  | Lista de ids de tablas de ruteo privadas  | NO  | []  |
| accepter_database_route_table_ids  | Lista de ids de tablas de ruteo de base de datos  | NO  | []  |
| cidr_blocks_accepters  | Direcciones ips de las vpc  | NO  | null  |
| tags  | Mapa de tags para asignar a los recuros de red  | NO  | null  |
| vpc_tags | Tags especificos para la vpc  | NO  | {}  |
| public_subnet_tags  | Tags especificos para la subnets publica  | NO  | {}}  |
| private_subnet_tags  | Tags especificos para la subnets privada  | NO  | {}}  |


### Outputs

| Nombre  | Descripción  |
|---|---|
| public_subnets  | Ids de las subnets publicas  |
| private_subnets  | Ids de las subnets privadas  |
| database_subnets  | Ids de las subnets de base de datos  |
| vpc_id  | Id de la vpc generada  |
| public_cidr_blocks  | Bloques cidr de las subnets publicas  |
| private_cidr_blocks  | Boques cidr de las subnets privadas  |
| database_cidr_blocks  | Bloques cidr de las subnets de la base de datos  |
| public_route_table_ids  | Ids de las tablas de ruteo publicas  |
| private_route_table_ids  | Ids de las tablas de ruteo privadas  |
| database_route_table_ids  | Ids de las tablas de ruteo de las subnet de la base de datos  |