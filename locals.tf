locals {
  config_requester_public = flatten([for route_table_id in module.aws_vpc.public_route_table_ids :
  [for index, value in aws_vpc_peering_connection.default : { prt_id = route_table_id, peering_index = index }]])

  config_requester_private = flatten([for route_table_id in module.aws_vpc.private_route_table_ids :
  [for index, value in aws_vpc_peering_connection.default : { prt_id = route_table_id, peering_index = index }]])

  config_requester_database = flatten([for route_table_id in module.aws_vpc.database_route_table_ids :
  [for index, value in aws_vpc_peering_connection.default : { drt_id = route_table_id, peering_index = index }]])

}
