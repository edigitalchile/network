module "aws_vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.64.0"
  name    = var.vpc_name
  cidr    = var.cidr
  azs     = compact(var.azs)

  private_subnets = concat(var.create_private_subnets ? [for index in range(length(compact(var.azs))) : cidrsubnet(var.cidr, var.private_bit_mask, var.private_subnet_start + index)] : [],
    var.create_secundary_private_subnets ? [for index in range(length(compact(var.azs))) : cidrsubnet(var.secondary_cidr_blocks, var.secundary_private_bit_mask, var.secundary_private_subnet_start + index)] : []
  )
  public_subnets   = var.create_public_subnets ? [for index in range(length(compact(var.azs))) : cidrsubnet(var.cidr, var.public_bit_mask, var.public_subnet_start + index)] : []
  database_subnets = var.create_database_subnets ? [for index in range(length(compact(var.azs))) : cidrsubnet(var.cidr, var.database_bit_mask, var.database_subnet_start + index)] : []

  #config for single nat gateway, nat is attached to firts public subnet
  enable_nat_gateway     = var.enable_nat_gateway
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = var.one_nat_gateway_per_az

  create_igw           = var.create_igw
  enable_dns_hostnames = var.enable_dns_hostnames

  vpc_tags            = var.vpc_tags
  public_subnet_tags  = var.public_subnet_tags
  private_subnet_tags = var.private_subnet_tags

  secondary_cidr_blocks = var.secondary_cidr_blocks != null ? [var.secondary_cidr_blocks] : []

  tags = var.tags
}

resource "aws_vpc_peering_connection" "default" {
  count       = var.create_peering ? length(var.peering_vpc_ids) : 0
  peer_vpc_id = var.peering_vpc_ids[count.index]
  vpc_id      = module.aws_vpc.vpc_id
  auto_accept = var.auto_accept_peering
  tags        = var.tags
}

resource "aws_route" "requester_public" {
  count                     = var.create_peering && var.peering_requester_public_route ? length(local.config_requester_public) : 0
  route_table_id            = local.config_requester_public[count.index].prt_id
  destination_cidr_block    = var.cidr_blocks_accepters[local.config_requester_public[count.index].peering_index]
  vpc_peering_connection_id = aws_vpc_peering_connection.default[local.config_requester_public[count.index].peering_index].id
}

resource "aws_route" "requester_private" {
  count                     = var.create_peering && var.peering_requester_private_route ? length(local.config_requester_private) : 0
  route_table_id            = local.config_requester_private[count.index].prt_id
  destination_cidr_block    = var.cidr_blocks_accepters[local.config_requester_public[count.index].peering_index]
  vpc_peering_connection_id = aws_vpc_peering_connection.default[local.config_requester_public[count.index].peering_index].id
}

resource "aws_route" "requester_database" {
  count                     = var.create_peering && var.peering_requester_database_route ? length(local.config_requester_database) : 0
  route_table_id            = local.config_requester_database[count.index].drt_id
  destination_cidr_block    = var.cidr_blocks_accepters[local.config_requester_public[count.index].peering_index]
  vpc_peering_connection_id = aws_vpc_peering_connection.default[local.config_requester_public[count.index].peering_index].id
}

resource "aws_route" "accepter_public" {
  count                     = var.create_peering && var.peering_accepter_public_route ? length(var.accepter_public_route_table_ids) : 0
  route_table_id            = var.accepter_public_route_table_ids[count.index]
  destination_cidr_block    = var.cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.default[count.index].id
}

resource "aws_route" "accepter_private" {
  count                     = var.create_peering && var.peering_accepter_private_route ? length(var.accepter_private_route_table_ids) : 0
  route_table_id            = var.accepter_private_route_table_ids[count.index]
  destination_cidr_block    = var.cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.default[count.index].id
}

resource "aws_route" "accepter_database" {
  count                     = var.create_peering && var.peering_accepter_database_route ? length(var.accepter_database_route_table_ids) : 0
  route_table_id            = var.accepter_database_route_table_ids[count.index]
  destination_cidr_block    = var.cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.default[count.index].id
}
