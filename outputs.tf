output public_subnets {
  value       = module.aws_vpc.public_subnets
  description = "Ids de las subnets publicas"
}

output private_subnets {
  value       = module.aws_vpc.private_subnets
  description = "Ids de las subnets privadas"
}

output database_subnets {
  value       = module.aws_vpc.database_subnets
  description = "Ids de las subnets de base de datos"
}

output vpc_id {
  value       = module.aws_vpc.vpc_id
  description = "Id de la vpc generada"
}

output public_cidr_blocks {
  value       = module.aws_vpc.public_subnets_cidr_blocks
  description = "Bloques cidr de las subnets publicas"
}

output private_cidr_blocks {
  value       = module.aws_vpc.private_subnets_cidr_blocks
  description = "Boques cidr de las subnets privadas"
}

output database_cidr_blocks {
  value       = module.aws_vpc.database_subnets_cidr_blocks
  description = "Bloques cidr de las subnets de la base de datos"
}

output public_route_table_ids {
  value       = module.aws_vpc.public_route_table_ids
  description = "Ids de las tablas de ruteo publicas"
}

output private_route_table_ids {
  value       = module.aws_vpc.private_route_table_ids
  description = "Ids de las tablas de ruteo privadas"
}

output database_route_table_ids {
  value       = module.aws_vpc.database_route_table_ids
  description = "Ids de las tablas de ruteo de las subnet de la base de datos"
}
