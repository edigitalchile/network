# REQUIRED PARAMETERS

variable azs {
  type        = list(string)
  description = "Lista de zonas de disponibilidad donde que se quieren crear recursos"

}

variable cidr {
  type        = string
  description = "Red de la vpc en formato x.x.x.x/x"
}

variable vpc_name {
  type        = string
  description = "Nombre de la vpc"
}


#OPTIONAL PARAMETERS

variable private_subnet_start {
  type        = number
  default     = 1
  description = "Punto de inicio de la subred privada"
}

variable secundary_private_subnet_start {
  type        = number
  default     = 1
  description = "Punto de inicio de la subred secundaria privada"
}

variable public_subnet_start {
  type        = number
  default     = 100
  description = "Punto de inicio de la subred public"
}

variable database_subnet_start {
  type        = number
  default     = 200
  description = "Punto de inicio de la subred public"
}

variable private_bit_mask {
  type        = number
  default     = 8
  description = "Mascara de la subred privada"
}
variable secundary_private_bit_mask {
  type        = number
  default     = 3
  description = "Mascara de la subred secundaria privada"
}

variable public_bit_mask {
  type        = number
  default     = 8
  description = "Mascara de la subred publica"
}

variable database_bit_mask {
  type        = number
  default     = 8
  description = "Mascara de la subred de la base de datos"
}

variable enable_dns_hostnames {
  type        = bool
  default     = true
  description = "Habilitar dns hostname en la vpc"
}

variable create_private_subnets {
  type        = bool
  default     = true
  description = "Habilitar la creacion de subredes privadas"
}

variable create_secundary_private_subnets {
  type        = bool
  default     = false
  description = "Habilitar la creacion de subredes privadas"
}

variable create_public_subnets {
  type        = bool
  default     = true
  description = "Habilitar la creacion de subredes publicas"
}

variable create_database_subnets {
  type        = bool
  default     = true
  description = "Habilitar la creacion de subredes privadas"
}

variable enable_nat_gateway {
  type        = bool
  default     = true
  description = "Habilitar creacion de la nat gateway"
}

variable single_nat_gateway {
  type        = bool
  default     = true
  description = "Configurar solo una nat gateway en toda la vpc"
}

variable one_nat_gateway_per_az {
  type        = bool
  default     = false
  description = "Configurar solo una nat gateway por zona de disponibilidad"
}

variable create_igw {
  type        = bool
  default     = true
  description = "Crear internet gateway"
}

variable create_peering {
  type        = bool
  default     = false
  description = "Crear peering entre dos vpc"
}

variable peering_vpc_ids {

  type        = list(string)
  default     = []
  description = "Lista de ids de las vpc que deben aceptar el peering"
}

variable auto_accept_peering {
  type        = bool
  default     = true
  description = "Aceptar automaticamente el peering. Las vpc deben estar en la misma cuenta aws"
}

variable peering_requester_public_route {
  type        = bool
  default     = false
  description = "Agregar ruta a tabla de ruteo publica de la vpc"
}

variable peering_requester_private_route {
  type        = bool
  default     = false
  description = "Agregar ruta a tabla de ruteo privada de la vpc"
}

variable peering_requester_database_route {
  type        = bool
  default     = false
  description = "Agregar ruta a tabla de ruteo de base de datos de la vpc"
}

variable peering_accepter_public_route {
  type        = bool
  default     = false
  description = "Agregar ruta a tabla de ruteo publica de la vpc"
}

variable peering_accepter_private_route {
  type        = bool
  default     = false
  description = "Agregar ruta a tabla de ruteo privada de la vpc"
}

variable peering_accepter_database_route {
  type        = bool
  default     = false
  description = "Agregar ruta a tabla de ruteo de la base de datos de la vpc"
}

variable accepter_public_route_table_ids {
  type        = list(string)
  default     = []
  description = "Lista de ids de tablas de ruteo publicas"
}

variable accepter_private_route_table_ids {
  type        = list(string)
  default     = []
  description = "Lista de ids de tablas de ruteo privadas"
}

variable accepter_database_route_table_ids {
  type        = list(string)
  default     = []
  description = "Lista de ids de tablas de ruteo de base de datos"
}

variable cidr_blocks_accepters {
  type        = list(string)
  default     = []
  description = "Direcciones ips de las vpc"
}

variable "secondary_cidr_blocks" {
  description = "CIDR block secundaria de la vpc"
  type        = string
  default     = null
}

variable tags {
  type        = map(string)
  description = "Mapa de tags para asignar a los recuros de red"
  default     = null
}

variable "vpc_tags" {
  description = "Tags especificos para la vpc"
  type        = map(string)
  default     = {}
}


variable "public_subnet_tags" {
  description = "Tags especificos para la subnets publica"
  type        = map(string)
  default     = {}
}

variable "private_subnet_tags" {
  description = "Tags especificos para la subnets privada"
  type        = map(string)
  default     = {}
}